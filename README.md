# gaussian

This is the implementation source code for the paper: 

FACCT: FAst, Compact, and Constant-Time Discrete Gaussian Sampler over Integers. (2019). Raymond K. Zhao, Ron Steinfeld, & Amin Sakzad. IEEE Transactions on Computers. DOI [10.1109/TC.2019.2940949](https://doi.org/10.1109/TC.2019.2940949).

To adapt the implementation to your target standard deviation sigma:

1. Change BINARY_SAMPLER_K to be the closest integer of sigma / sqrt(1 / (2 * ln2)) i.e. BINARY_SAMPLER_K = round(sigma / sqrt(1 / (2 * ln2)))
2. __Important:__ Change the uniform_sampler function in order to sample an integer in \[0, BINARY_SAMPLER_K - 1\]. An example for BINARY_SAMPLER_K = 2411 i.e. sigma = 2048 is given as follows. 
The uniform_sampler function will first get an integer x in \[0, UNIFORM_Q - 1\], where UNIFORM_Q is an integer multiple of BINARY_SAMPLER_K and close to 2<sup>8 * UNIFORM_SIZE</sup>. 
Then the sampler will perform x mod BINARY_SAMPLER_K in order to get a uniform sample in \[0, BINARY_SAMPLER_K - 1\].

Note: The UNIFORM_REJ is calculated in the same method as in the [Titanium KEM](http://users.monash.edu.au/%7Erste/Titanium_v11.pdf) (see page 53 in section 5.1.2) to guarantee that the probability of rerunning the pseudorandom generator due to insufficient randomness is less than 2<sup>-64</sup>. 

```c
#define UNIFORM_SIZE 2
#define UNIFORM_REJ 20
#define BARRETT_BITSHIFT (UNIFORM_SIZE * 8)

#define BARRETT_FACTOR ((1LL << BARRETT_BITSHIFT) / BINARY_SAMPLER_K)
#define UNIFORM_Q (BINARY_SAMPLER_K * BARRETT_FACTOR)

/* make sure that Pr(rerun the PRG)<=2^(-64) */
static inline void uniform_sampler_ref(unsigned char *r, uint64_t *y)
{
	uint32_t i = 0, j = 0;
	uint64_t x;
	
	while (j < 8)
	{
		do
		{	/* we ignore the low probability of rerunning the PRG 
			 * change the loading size i.e. uint16_t according to your UNIFORM_SIZE */
			x = *((uint16_t *)(r + UNIFORM_SIZE * (i++)));
		} while (1 ^ ((x - UNIFORM_Q) >> 63));

		x = x - ((((x * BARRETT_FACTOR) >> BARRETT_BITSHIFT) + 1) * BINARY_SAMPLER_K);
		x = x + (x >> 63) * BINARY_SAMPLER_K;
		
		y[j++] = x;
	}	
}

static inline void uniform_sampler_avx(unsigned char *r, __m256i *y1, __m256i *y2)
{
	uint64_t sample[8] __attribute__ ((aligned (32)));
	uint32_t i = 0, j = 0;
	uint64_t x;
	
	while (j < 8)
	{
		do
		{	/* we ignore the low probability of rerunning the PRG 
			 * change the loading size i.e. uint16_t according to your UNIFORM_SIZE */
			x = *((uint16_t *)(r + UNIFORM_SIZE * (i++)));
		} while (1 ^ ((x - UNIFORM_Q) >> 63));

		x = x - ((((x * BARRETT_FACTOR) >> BARRETT_BITSHIFT) + 1) * BINARY_SAMPLER_K);
		x = x + (x >> 63) * BINARY_SAMPLER_K;
		
		sample[j++] = x;
	}
	
	*y1 = _mm256_load_si256((__m256i *)(sample));
	*y2 = _mm256_load_si256((__m256i *)(sample + 4));
}
```
